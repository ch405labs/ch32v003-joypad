#include "ch32v003_joypad.h"

// Global variable
static ControllerState controller_states;

/**
 * @brief Initialise controller driver
 * 
 * Initialises the controller driver by settiung up the gpios where the 
 * controllers are connected and initialising the analog to digital converter.
 */
void controller_init(void) {

    // Configure the controller 1 + 2 as an analog input
    GPIO_port_enable(PORT_CTRL);
    GPIO_pinMode(GPIOv_from_PORT_PIN(PORT_CTRL, PIN_CTRL1), GPIO_pinMode_I_analog, GPIO_Speed_10MHz);
    GPIO_pinMode(GPIOv_from_PORT_PIN(PORT_CTRL, PIN_CTRL2), GPIO_pinMode_I_analog, GPIO_Speed_10MHz);  
    GPIO_ADCinit();

    // Prepare controller states
    controller_states.current = 0;
    controller_states.previous = 0;
    controller_states.repeat = 0;
}

/**
 * @brief Check if up button is pressed
 * 
 * This function checkes it the up button is pressed. For this it verifies
 * if the analog value lies between any of the upper and lower boundaries for
 * any combination of button presses where the action button is also pressed.
 * 
 * @param ain_ctrl      The analog input for the controller (use CTRLX define)
 * @return 0 if up button is not pressed, any other value otherwise.
 */
int8_t controller_up_pressed(enum GPIO_analog_inputs ain_ctrl) {
    if(ain_ctrl == CTRL1) {
        return (controller_states.current & CTRL_ST_P1UP)>>0;
    } else if(ain_ctrl == CTRL2) {
        return (controller_states.current & CTRL_ST_P2UP)>>3;
    } else {
        return -1;
    }   
}

/**
 * @brief Check if down button is pressed
 * 
 * This function checkes it the down button is pressed. For this it verifies
 * if the analog value lies between any of the upper and lower boundaries for
 * any combination of button presses where the action button is also pressed.
 * 
 * @param ain_ctrl      The analog input for the controller (use CTRLX define)
 * @return 0 if down button is not pressed, any other value otherwise.
 */
int8_t controller_down_pressed(enum GPIO_analog_inputs ain_ctrl) {
    if(ain_ctrl == CTRL1) {
        return (controller_states.current & CTRL_ST_P1DOWN)>>1;
    } else if(ain_ctrl == CTRL2) {
        return (controller_states.current & CTRL_ST_P2DOWN)>>4;
    } else {
        return -1;
    }
}

/**
 * @brief Check if action button is pressed
 * 
 * This function checkes it the action button is pressed. For this it verifies
 * if the analog value lies between any of the upper and lower boundaries for
 * any combination of button presses where the action button is also pressed.
 * 
 * @param ain_ctrl      The analog input for the controller (use CTRLX define)
 * @return 0 if action button is not pressed, -1 on error, any other value 
 *          if button was pressed on last capture.
 */
int8_t controller_act_pressed(enum GPIO_analog_inputs ain_ctrl) {
    if(ain_ctrl == CTRL1) {
        return (controller_states.current & CTRL_ST_P1ACT)>>2;
    } else if(ain_ctrl == CTRL2) {
        return (controller_states.current & CTRL_ST_P2ACT)>>5;
    } else {
        return -1;
    }
}

/**
 * @brief Detects a unpressed to pressed state change
 */
int8_t controller_rising(uint16_t mask) {
    return ((!(controller_states.previous & mask)) && (controller_states.current & mask));
}

/** @brief The number of times this button is pushed (at least one if pushed, otherwise 0) */
int8_t controller_repeats(uint16_t mask) {
    return ((controller_states.current & mask) ? (controller_states.repeat+1 ? controller_states.repeat+1 : 1) : 0);
}

/**
 * @brief Capture the state of the controller
 */
void controller_capture() {
    controller_states.previous = controller_states.current;
    uint16_t val = GPIO_analogRead(CTRL1);
    controller_states.current = (
        (val_valid(val, CTRL_UP) * (CTRL_ST_P1UP)) |
        (val_valid(val, CTRL_DOWN) * (CTRL_ST_P1DOWN)) |
        (val_valid(val, CTRL_ACT) * (CTRL_ST_P1ACT)) |
        (val_valid(val, CTRL_UPDOWN) * (CTRL_ST_P1UP|CTRL_ST_P1DOWN)) |
        (val_valid(val, CTRL_UPACT) * (CTRL_ST_P1UP|CTRL_ST_P1ACT)) |
        (val_valid(val, CTRL_DOWNACT) * (CTRL_ST_P1DOWN|CTRL_ST_P1ACT)) |
        (val_valid(val, CTRL_UPDOWNACT) * (CTRL_ST_P1UP|CTRL_ST_P1DOWN|CTRL_ST_P1ACT))
    );
    val = GPIO_analogRead(CTRL2);
    controller_states.current |= (
        (val_valid(val, CTRL_UP) * (CTRL_ST_P2UP)) |
        (val_valid(val, CTRL_DOWN) * (CTRL_ST_P2DOWN)) |
        (val_valid(val, CTRL_ACT) * (CTRL_ST_P2ACT)) |
        (val_valid(val, CTRL_UPDOWN) * (CTRL_ST_P2UP|CTRL_ST_P2DOWN)) |
        (val_valid(val, CTRL_UPACT) * (CTRL_ST_P2UP|CTRL_ST_P2ACT)) |
        (val_valid(val, CTRL_DOWNACT) * (CTRL_ST_P2DOWN|CTRL_ST_P2ACT)) |
        (val_valid(val, CTRL_UPDOWNACT) * (CTRL_ST_P2UP|CTRL_ST_P2DOWN|CTRL_ST_P2ACT))
    );
    if(controller_states.current != controller_states.previous)
        controller_states.repeat = 0;
    else
        ++controller_states.repeat;
}