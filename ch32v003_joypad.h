/**
 * @file    ch32v003_joypad.h
 * @author  Florian Schütz (fschuetz@ieee.org), Miaou
 * @brief   A driver for the BalCCon Cyberdeck Companion Badge 0o00 keypad
 * @version 0.1
 * @date    26.01.2024
 * 
 * @copyright Copyright (c) 2023 Florian Schütz, released under MIT license
 * 
 * This driver reads the button state of the two three button controllers of the
 * Balccon Cyberdeck Companion Badge 0o00. A readout can be triggered by calling
 * the relatvie functions.
 * 
 * This driver requires a controller to be connected to one gpio. The controller
 * should reflect button presses by generating different analogue voltages. The
 * driver reads these voltages at the gpio and deduces which buttons where 
 * pressed.
 * 
 * @todo 
 *      - Make number of buttons configurable through define to make it more 
 *              generic.
 *      - Allow different ports for each controller
 *      - Remove requirement to define ain value and gpio number for each 
 *              controller.
 *      - Support more (or less) than two controllers.
 *
 * Based on Tiny Joypad Drivers for CH32V003 v1.0, 2023 by Stefan Wagner 
 * https://github.com/wagiminator
 */

#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "ch32v003fun.h"
#include "ch32v003_GPIO_branchless.h"

// Port/Pin assignments and controler ids
#define PORT_CTRL       GPIO_port_D                                             /**< Port of the controller */
#define PIN_CTRL1       3                                                       /**< GPIO of controller 1 */
#define AIN_CTRL1       GPIO_Ain4_D3                                            /**< GPIO of controller 1 as ain value */
#define PIN_CTRL2       4                                                       /**< GPIO of controller 2 */
#define AIN_CTRL2       GPIO_Ain7_D4                                            /**< GPIO of controller 2 as ain value */

#define CTRL1           AIN_CTRL1                                               /**< Synonym for code readability */                                               
#define CTRL2           AIN_CTRL2                                               /**< Synonym for code readability */

#define ID_CTRL1        0                                                       /**< Controller 1 ID */
#define ID_CTRL2        1                                                       /**< Controller 2 ID */

// Button calibration values
//
// Button Values are for a three button pad with a base resistor of 2k and the
// button resistors of 6k8 (UP), 3k9 (DOWN) and 2k (ACT).
#define CTRL_NONE       0                                                       /**< No key pressed */
#define CTRL_UP         233                                                     /**< Key up pressed */
#define CTRL_DOWN       347                                                     /**< Key down pressed */
#define CTRL_ACT        511                                                     /**< Key action pressed */
#define CTRL_UPDOWN     457                                                     /**< Key up + down pressed */
#define CTRL_UPACT      577                                                     /**< Key up + action pressed */
#define CTRL_DOWNACT    616                                                     /**< Key down + action pressed */
#define CTRL_UPDOWNACT  659                                                     /**< Key up+down+action pressed */
#define CTRL_DEV         15                                                     /**< deviation */

// Current and previous states are stored in an uint16_t that we pack with the 
// following flags
#define CTRL_ST_P1UP   (1<<0)
#define CTRL_ST_P1DOWN (1<<1)
#define CTRL_ST_P1ACT  (1<<2)
#define CTRL_ST_P2UP   (1<<3)
#define CTRL_ST_P2DOWN (1<<4)
#define CTRL_ST_P2ACT  (1<<5)

// Helper to determine if a value of a button read is valid
#define val_valid(val, tgt) ((val > tgt - CTRL_DEV) && (val < tgt  + CTRL_DEV))

/**
 * @brief Structure to capture current and previous state of controller
 * 
 * The field current contains the current state, while the filed previous 
 * contains the state before the last capture. Repeat will count how many 
 * times the state was repeated (no change when capturing ne state to the
 * previous state).
 */
typedef struct {
    uint8_t current;                                                            /**< Current state of the buttons */
    uint8_t previous;                                                           /**< Previous state of the buttons */
    uint16_t repeat;                                                            /**< Number of calls wher current == previous */
} ControllerState;

/**
 * @brief Initialise controller driver
 * 
 * Initialises the controller driver by settiung up the gpios where the 
 * controllers are connected and initialising the analog to digital converter.
 */
void controller_init(void);

// Buttons
#define controller_act_released(ain_ctrl)  (!controller_act_pressed(ain_ctrl))
#define controller_pad_pressed(ain_ctrl)   (GPIO_analogRead(ain_ctrl) > CTRL_DEV)      
#define controller_pad_released(ain_ctrl)  (GPIO_analogRead(ain_ctrl) <= CTRL_DEV)     
#define controller_all_released()          (controller_pad_released(AIN_CTRL1) && contoller_pad_released(AIN_CTRL2))

/**
 * @brief Capture the state of the controller
 */
void controller_capture();

/**
 * @brief Check if up button is pressed
 * 
 * This function checkes it the up button is pressed. For this it verifies
 * if the analog value lies between any of the upper and lower boundaries for
 * any combination of button presses where the action button is also pressed.
 * 
 * @param ain_ctrl      The analog input for the controller (use CTRLX define)
 * @return 0 if up button is not pressed, any other value otherwise.
 */
int8_t controller_up_pressed(enum GPIO_analog_inputs ain_ctrl);

/**
 * @brief Check if down button is pressed
 * 
 * This function checkes it the down button is pressed. For this it verifies
 * if the analog value lies between any of the upper and lower boundaries for
 * any combination of button presses where the action button is also pressed.
 * 
 * @param ain_ctrl      The analog input for the controller (use CTRLX define)
 * @return 0 if down button is not pressed, any other value otherwise.
 */
int8_t controller_down_pressed(enum GPIO_analog_inputs ain_ctrl);

/**
 * @brief Check if action button is pressed
 * 
 * This function checkes it the action button is pressed. For this it verifies
 * if the analog value lies between any of the upper and lower boundaries for
 * any combination of button presses where the action button is also pressed.
 * 
 * @param ain_ctrl      The analog input for the controller (use CTRLX define)
 * @return 0 if action button is not pressed, any other value otherwise.
 */
int8_t controller_act_pressed(enum GPIO_analog_inputs ain_ctrl); 

/**
 * @brief Detects rising edge (event button pressed)
 * 
 * The rising edge is "was not" and "is now".
 */
//#define controller_rising(mask) ((!(controller_states.previous & mask)) && (controller_states.current & mask))
int8_t controller_rising(uint16_t mask);

/** @brief The number of times this button is pushed (at least one if pushed, otherwise 0) */
int8_t controller_repeats(uint16_t mask);
//#define controller_repeats(state, mask) ((state.current & mask) ? (state.repeat+1 ? state.repeat+1 : 1) : 0)

#ifdef __cplusplus
};
#endif
